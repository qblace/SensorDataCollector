package com.size;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.billionav.navi.sensor.LocSnsListener;
import com.billionav.navi.gps.LocGpsListener;

public class MainActivity extends AppCompatActivity {

    Button m_btnStart, m_btnStop;

    int aaaa;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    protected String getCurProcessName(Context context) {
        int pid = android.os.Process.myPid();
        ActivityManager mActivityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess : mActivityManager
                .getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LocGpsListener.instance();
        LocSnsListener.instance().initialize(this);
//
        LocSnsListener.instance().start();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        final TextView tv = (TextView) findViewById(R.id.sample_text);
        tv.setText(stringFromJNI());

        aaaa = 0;

        // start service button
        m_btnStart = (Button) findViewById(R.id.m_btnStart);
        m_btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startServiceIntent = new Intent(MainActivity.this, SensorDataCollectionService.class);
                startService(startServiceIntent);
                // Log.d("sns", "start service button clicked ===>" + Thread.currentThread().getId());
                Log.d("sns", "start service button clicked Pid ===> " + android.os.Process.myPid() + " Tid ===> " + Thread.currentThread().getId());
                aaaa++;
                tv.setText(Integer.toString(aaaa));
            }
        });

        // stop service button
        m_btnStop = (Button) findViewById(R.id.m_btnStop);
        m_btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent stopServiceIntent = new Intent(MainActivity.this, SensorDataCollectionService.class);
                stopService(stopServiceIntent);
                Log.d("sns", "stop service button clicked ===>" + Thread.currentThread().getId());
                aaaa--;
                tv.setText(Integer.toString(aaaa));
            }
        });
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
}
