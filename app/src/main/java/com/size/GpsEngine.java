package com.size;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;

/**
 * Created by qibaolei on 2017/12/14.
 */

public class GpsEngine {
    // 成员变量
    private LocationManager m_locationManager = null;
    private Location m_location = null;
    private Context m_context = null;

    // 构造函数
    public GpsEngine(Context context) {
        m_context = context;
        m_locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        m_location = m_locationManager.getLastKnownLocation();
    }

    // 获取最佳location provider
    public String getBestLocationProvider() {
        // 设置获取最佳Location provider的条件
        Criteria criteria = new Criteria();
        // 查询精度：ACCURACY_COARSE（粗略）、ACCURACY_FINE（准确）、ACCURACY_HIGH（高）、 ACCURACY_MEDIUM（中）、ACCURACY_LOW（低）
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        // 电量要求：POWER_HIGH（高）、POWER_MEDIUM（中）、POWER_LOW（低）
        criteria.setPowerRequirement(Criteria.POWER_MEDIUM);
        // 是否允许付费：true（是）、 false（否）
        criteria.setCostAllowed(true);
        // 是否查询海拔：true（是）、 false（否）
        criteria.setAltitudeRequired(true);
        // 是否查询车速：true（是）、 false（否）
        criteria.setSpeedRequired(true);
        // 是否查询方位角：true（是）、 false（否）
        criteria.setBearingRequired(true);
    }
}
