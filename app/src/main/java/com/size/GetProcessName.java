//package com.size;
//
///**
// * Created by qibaolei on 2017/12/13.
// */
//
//public class GetProcessName {
// /**
//  * 根据Pid获取当前进程的名字，一般就是当前app的包名
//  *
//  * @param context 上下文
//  * @param pid 进程的id
//  * @return 返回进程的名字
//  */
// public String getAppName(Context context, int pid)
// {
//     ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//     List list = activityManager.getRunningAppProcesses();
//     Iterator i = list.iterator();
//     while (i.hasNext())
//     {
//         ActivityManager.RunningAppProcessInfo info = (ActivityManager.RunningAppProcessInfo) (i.next());
//         try
//         {
//             if (info.pid == pid)
//             {
//                 // 根据进程的信息获取当前进程的名字
//                 return info.processName;
//             }
//         }
//         catch (Exception e)
//         {
//             e.printStackTrace();
//         }
//     }
//     // 没有匹配的项，返回为null
//     return null;
// }
//}
