package com.size;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.IBinder;
import android.util.Log;

public class SensorDataCollectionService extends Service {

    LocationManager m_GpsManager = null;
    public SensorDataCollectionService() {
        super();
    }

    public void onCreate() {
        Log.d("snsService", "service function onCreate() is called ===>" + Thread.currentThread().getId());
        // throw new RuntimeException("Stub!");
        m_GpsManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        // Log.d("snsService", "service function onStartCommand() is called ===>" + Thread.currentThread().getId());
        Log.d("snsService", "service function onStartCommand() is called Pid ===> " + android.os.Process.myPid() + " Tid ===> " + Thread.currentThread().getId());
        return START_STICKY;
        // throw new RuntimeException("Stub!");
    }

    public void onDestroy() {
        Log.d("snsService", "service function onDestroy() is called ===>" + Thread.currentThread().getId());
        // throw new RuntimeException("Stub!");
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("snsService", "service function onBind() is called ===>" + Thread.currentThread().getId());
        return null;
    }

    public boolean onUnbind(Intent intent) {
        Log.d("snsService", "service function onUnbind() is called ===>" + Thread.currentThread().getId());
        return true;
    }

    public void onRebind(Intent intent) {
        Log.d("snsService", "service function onRebind() is called ===>" + Thread.currentThread().getId());
        // throw new RuntimeException("Stub!");
    }
}
